
# A url shortener/encoder

## design

This code takes a passed url from a POST and shortens/encodes it.  The smallest 
of urls may be come a little larger, but largest ones will definitely become much
smaller

The logic flow is that the url that is posted from the form is first
hashed via murmer3 to get a 32 bit value.   This value then converted 
via base 62 encoding to a string which is the tinyURI

eg, http://mytiny.url/E4rFQb where E4rFQb is the base 62  / uri 'key'

The key is then stored in an in memory key value store MapDB backed by disk with the shortened version as the key
and the long version as the value.   The reverse is stored also to lookup if we have already shortened a url.

The reverse of this logic is when a tinyurl is invoked it is ingested by the GET method of the server.
The uri is used to lookup the full url in the store.   Once found a redirect is returned on the fullURL.

THe main class starts the jetty server which contains one servlet, the URLServlet that handles this exercise;

## design issues
The size of the hash.   To support a larger keyspace murmur3_128 could have been used to support far more tinyurls.
this has the disadvantage of creating larger tinyurls.    Which at least looks uglier and is odd when you consider a.com turns into a 
12 digit tinyurl.   This is possible enhancement.

Mapdb currently uses 32 bits to hash, tho its is planned to go to 64 in the future.

A regurlar 32 bit hashmap is wasted on a larger address space with trillions or more possible entries.  An external
store such as a SQL DB or a key value store like redis would be optimum.  But those don't fit neatly into small
code projects delivered as git projects. The question of what to store in arises.   There are several options such as mapdb, insight, chronicle, berkelydb.   I went with mapDB as
it fits the bill for a key value store with the added benefit of being persistent across reboots.

## build
The code is built using gradle, my build tool of choice.   It handles all the dependencies on guava, jetty, and mapdb.

build with 

$ gradle build jar

The executable jar is **tinyurl-1.0-SNAPSHOT.jar**