package com.roger.apple;


import static org.junit.Assert.*;

import com.roger.apple.main.Shortener;
import org.junit.Test;

public class ShortenTest {

    @Test
    public void testReversals() {

        Shortener s = new Shortener();

        assertEquals(s.shorten(null), "");
        assertEquals(s.shorten(""), "");
        assertEquals(s.shorten("a"), "EBZNP");
        assertEquals(s.shorten("aa"),"cgHLDq" );
        assertEquals(s.shorten("http://www.amazon.com"), "E38xr");
        assertEquals(s.shorten("https://www.reddit.com/r/Archaeology/comments/bfe18e/porth_hellick_burial_chamber_built_around_4000/"),"bZ9Ymq");
        assertEquals(s.shorten("https://ignite.apache.org/use-cases/database/key-value-store.html"), "fvXQd");
        assertEquals(s.shorten("https://www.google.com/search?ei=EoO7XJ-BEsPM0PEPxt-q8Aw&q=httpservletresponse+jetty+setresponse&oq=httpservletresponse+jetty+setresponse&gs_l=psy-ab.3..33i160.2092.3517..3927...0.0..0.302.2666.0j4j7j1......0....1..gws-wiz.......0i22i30j33i299.Ov53iotMuaA"), "bzg6Fk");
        assertEquals(s.shorten("http://a.io"), "bA4RcC");
    }
}
