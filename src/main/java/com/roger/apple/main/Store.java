package com.roger.apple.main;

import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;

public class Store {

    private static Store me = new Store();

    private DB db = DBMaker.memoryDB().make();
    private HTreeMap<String, String> tinyToFull = db.hashMap("tinyMap")
            .keySerializer(Serializer.STRING)
            .valueSerializer(Serializer.STRING)
            .create();

    private HTreeMap<String, String> fullToTiny = db.hashMap("fullMap")
            .keySerializer(Serializer.STRING)
            .valueSerializer(Serializer.STRING)
            .create();

    private Store() {
    }

    /**
     * @param key   tinyurl
     * @param value fullurl
     */
    public void storeTiny(String key, String value) {
        if (key == null || value == null)
            return;

        tinyToFull.put(key, value);
    }

    /**
     * @param key tinyurl
     * @return fullurl
     */
    public String lookupTiny(String key) {
        return tinyToFull.get(key);
    }



    /**
     * @param key   fullurl
     * @param value tinyurl
     */
    public void storeFull(String key, String value) {
        if (key == null || value == null)
            return;

        fullToTiny.put(key, value);
    }

    /**
     * @param key fullurl
     * @return tinyurl
     */
    public String lookupFull(String key) {
        return fullToTiny.get(key);
    }

    public static Store getInstance() {
        return me;
    }


    public static void main (String[] args) {
        Store s = Store.getInstance();

        s.storeTiny("abc", "asdfasdfasdf");

        System.out.println(s.lookupTiny("abc"));
    }
}
