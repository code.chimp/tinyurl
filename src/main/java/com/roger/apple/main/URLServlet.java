package com.roger.apple.main;

import org.apache.commons.validator.routines.UrlValidator;
import org.eclipse.jetty.http.HttpStatus;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;

public class URLServlet extends HttpServlet {

    Shortener shorter = new Shortener();
    Store storage = Store.getInstance();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String tinyURL = request.getRequestURI();

        if (tinyURL == null) {
            response.setStatus(HttpStatus.NOT_FOUND_404);
            Writer w = response.getWriter();

            if (tinyURL == null)
                w.append("missing tinyurl");
            w.append("</html>");
            w.close();
            return;
        }

        tinyURL = tinyURL.replaceFirst("/", "");
        String redirectURL = storage.lookupTiny(tinyURL);

        if (redirectURL == null) {
            response.setStatus(HttpStatus.NOT_FOUND_404);
            Writer w = response.getWriter();
            w.append("<html>no value found</html>");
            return;
        }
        response.setContentType("text/html");
        response.sendRedirect(redirectURL);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String fullURL = request.getParameter("url");

        if (fullURL == null  || ! isValidURL(fullURL)) {
            response.setStatus(HttpStatus.BAD_REQUEST_400);
            Writer w = response.getWriter();
            w.append("<html>");
            if (fullURL == null)
                w.append("missing url to shorten");
            else
                w.append("invalid url ").append(fullURL).append(" to shorten");
            w.append("</html>");
            w.close();
            return;
        }

        String tinyUrl = createTiny(fullURL);
        response.setStatus(HttpStatus.OK_200);
        Writer write = response.getWriter();
        write.append("<HTML>Tiny URL: " + tinyUrl);
        write.close();
        response.setContentType("text/html");

    }

    String createTiny(String fullUrl) {

        // check to see if we have stored this before
        if (storage.lookupFull(fullUrl) != null)
            return storage.lookupFull(fullUrl);

        String tiny = shorter.shorten(fullUrl);

        storage.storeTiny(tiny, fullUrl);
        storage.storeFull(fullUrl, tiny);


        String tinyUrl = "localhost:80/" + tiny;

        return tinyUrl;
    }

    boolean isValidURL(String url) {
            UrlValidator urlValidator = new UrlValidator();
            return urlValidator.isValid(url);
    }
}
