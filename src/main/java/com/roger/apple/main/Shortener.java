package com.roger.apple.main;

import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

import java.math.BigInteger;


public class Shortener {

    final char[] map = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
    final BigInteger BASE = BigInteger.valueOf(map.length);
    final HashFunction hasher = Hashing.murmur3_32();

    public String shorten(String url) {
        if (url == null || url.isEmpty())
            return "";

        HashCode code = hasher.hashUnencodedChars(url);

        var shorter = new StringBuilder();
        BigInteger n = new BigInteger(code.asBytes()).abs();

        while (n.compareTo(BigInteger.ZERO) > 0) {

            var x = n.remainder(BASE).intValue();
            shorter.append(map[x]);
            n = n.divide(BASE);
        }

        // Reverse shortURL to complete base conversion
        shorter.reverse();

        return shorter.toString();
    }


    // test

    public static void main(String[] args) {

        var s = new Shortener();

        System.out.println(s.shorten(null));
        System.out.println(s.shorten(""));
        System.out.println(s.shorten("a"));
        System.out.println(s.shorten("aa"));
        System.out.println(s.shorten("http://www.amazon.com"));
        System.out.println(s.shorten("https://www.reddit.com/r/Archaeology/comments/bfe18e/porth_hellick_burial_chamber_built_around_4000/"));
        System.out.println(s.shorten("https://ignite.apache.org/use-cases/database/key-value-store.html"));
        System.out.println(s.shorten("https://www.google.com/search?ei=EoO7XJ-BEsPM0PEPxt-q8Aw&q=httpservletresponse+jetty+setresponse&oq=httpservletresponse+jetty+setresponse&gs_l=psy-ab.3..33i160.2092.3517..3927...0.0..0.302.2666.0j4j7j1......0....1..gws-wiz.......0i22i30j33i299.Ov53iotMuaA"));
        System.out.println(s.shorten("http://a.io"));

    }
}
